class CreateWishes < ActiveRecord::Migration
  def change
    create_table :wishes do |t|
      t.text :text
      t.integer :user_id

      t.timestamps
    end
  end
end
