class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
   :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]


  belongs_to :santa, class_name: "User"
  has_one :child, class_name: "User", foreign_key: "santa_id"
  has_many :wishes

  validates :santa_id, uniqueness: true, allow_nil: true

  def self.from_omniauth(auth)
  where(provider: auth.provider, uid: auth.uid).first#_or_create do |user|
#    user.email = auth.info.email
#    user.password = Devise.friendly_token[0,20]
#    user.name = auth.info.name   # assuming the user model has a name
#    user.remote_avatar_url =  auth.info.image
#  end
end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end
