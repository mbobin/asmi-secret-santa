class WishesController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @wishes = current_user.wishes
  end

  def new
    @wish = current_user.wishes.new
  end

  def create
    @wish = current_user.wishes.new(wish_params)
    if @wish.save
      redirect_to root_path, notice: "Wish added! Let's hope someone will make it happen."
    else
      render :new
    end
  end

  private
    def set_wish
      @wish = current_user.wishes.find(params[:id])
    end

    def wish_params
      params.require(:wish).permit(:text)
    end

end
