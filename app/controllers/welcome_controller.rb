class WelcomeController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @santa = current_user.santa
  end

  def set_santa
    current_user.santa = find_santa if current_user.santa == nil
    if current_user.save
      redirect_to root_path
    else
      redirect_to root_path, alert: "Something went wrong. Please try again."
    end
  end

  private
    def find_santa
      attr_users = User.where.not(santa_id: nil).pluck(:santa_id)
      attr_users << current_user.id
      avalaible_users =  User.all - User.find(attr_users)
      avalaible_users.sample
    end
end
